#!/bin/bash

# On mydocker
#export PASSWORD=$2
# local
export PASSWORD="password"

cp -Rn /home/initialWorkspace/.zshrc             /home/workspace/
cp -Rn /home/initialWorkspace/.openvscode-server /home/workspace/.openvscode-server

/home/.openvscode-server/bin/openvscode-server \
    --host 0.0.0.0 \
    --default-folder /home/workspace \
    --without-connection-token
   &

token="$PASSWORD" secure=0 staticfolder="/home/gen-http-proxy/node_modules/gen-http-proxy/static" /home/gen-http-proxy/node_modules/.bin/gen-http-proxy :3010 0.0.0.0:3000
