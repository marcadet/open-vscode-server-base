#
# This is the base image for a development environment that can be deployed on MyDocker
#
# Requirements:
# - image must be useable on MyDocker
#   - a password will be needed to access the environment
#   - a wrapper_script.sh is used to start the environment
# - image must be useable locally on a computer with docker
#   - password is not needed in this case
#
# Needed tools:
# - a source code editor with access to a shell through a Terminal
#   - known tools (10/08/2022)
#     - https://github.com/gitpod-io/openvscode-server
#       + up to date with desktop VSCode (1.70.0)
#       + based on Ubuntu 22.04 through buildpack-deps:22.04-curl (https://hub.docker.com/_/buildpack-deps/)
#       - no password option, need a proxy
#         - try with gen-http-proxy, several problems (may be others)
#           - Terminal not useable in Safari (OK with Firefox)
#           - Descriptions of extensions not displayed
#         - try with my own proxy: https://github.com/marcadetd/python-reverse-proxy
#           - still not OK
#         - just use the ask_password part of my proxy before running VSCode
#           - one has to reload the page
#     - https://github.com/linuxserver/docker-code-server
#       - not up to date with desktop VSCode
#       - based on Ubuntu 20.04
#       + password option available
#     - https://github.com/theia-ide/theia-apps
#       - "This repository is soon to be considered deprecated"
#         "Theia Blueprint (https://github.com/eclipse-theia/theia-blueprint) is a more polished example"
#         "We are working on a browser version of Blueprint"
# - git
# - one to download files (wget, curl)
#
# Wanted tools:
# - zsh, oh-my-zsh
# - python3 (always useful, even if not used as the environment programming language)
#     and needed for my reverse-proxy
#
#

FROM gitpod/openvscode-server:latest

## to get permissions to install packages and such
USER root

## software needed

# git, curl already installed

RUN apt-get update && apt-get install -y --no-install-recommends \
        python3 \
        python3-pip \
        zsh \
    && rm -rf /var/lib/apt/lists/* \
    && chsh --shell /bin/zsh openvscode-server

# used by ask_password
RUN pip3 install bottle

## wrapper_script needed for MyDocker

# Will be entry point
COPY wrapper_script.sh /usr/local/lib/wrapper_script.sh
ENTRYPOINT ["/bin/bash", "/usr/local/lib/wrapper_script.sh"]

COPY ask_password.py /usr/local/lib/ask_password.py

# Initial files are in initialWorkspace, they are copied by wrapper_script.sh if not already present in workspace 
RUN    mkdir /home/initialWorkspace \
    && chown -R openvscode-server:openvscode-server /home/initialWorkspace

## user installations
USER openvscode-server

# hide some files in VSCode explorer vue
RUN mkdir -p /home/initialWorkspace/.openvscode-server/data/Machine/
COPY settings.json /home/initialWorkspace/.openvscode-server/data/Machine/

# Install on-my-zsh
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
COPY .zshrc /home/initialWorkspace/

# Python extension
RUN     mkdir -p /home/initialWorkspace/.openvscode-server/extensions \
    && /home/.openvscode-server/bin/openvscode-server \
        --extensions-dir /home/initialWorkspace/.openvscode-server/extensions \
        --install-extension ms-python.python

WORKDIR /home/workspace/
