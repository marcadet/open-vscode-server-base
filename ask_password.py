#!/usr/bin/env python3

import sys
from threading import Thread, Lock

from bottle import Bottle, ServerAdapter, request, redirect

password = None

# Ideas from https://stackoverflow.com/questions/11282218/bottle-web-framework-how-to-stop
class LoginServer(ServerAdapter):
    server = None

    def run(self, handler):
        from wsgiref.simple_server import make_server
        self.server = make_server(self.host, self.port, handler, **self.options)
        self.server.serve_forever()

    def stop(self):
        self.server.shutdown()
        self.server.server_close()

app = Bottle()

@app.route('/')
def login():
    return '''
        <form action="/" method="post">
            Password: <input name="password" type="password" />
            <input value="Login" type="submit" />
        </form>
    '''

login_server = LoginServer(host='0.0.0.0', port=3000)
signal_to_stop = Lock()

@app.route('/', method='POST')
def do_login():
    global password
    global signal_to_stop
    if str(request.forms.get('password')) == password:
        signal_to_stop.release()
    redirect('/')

def login_begin():
    app.run(server=login_server)

def login_stop():
    global signal_to_stop
    global login_server
    signal_to_stop.acquire()
    login_server.stop()

def main():
    global password, signal_to_stop, login_server
    if len(sys.argv) > 1:
        password = sys.argv[1]
        signal_to_stop.acquire()
        Thread(target=login_begin).start()
        signal_to_stop.acquire()
        login_server.stop()

if __name__ == '__main__':
    main()
